#ifndef DBCONNECT_H
#define DBCONNECT_H

#include <QDialog>
#include <QDebug>
#include <QSettings>
#include <QComboBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QMessageBox>
#include <QtSql/QSqlError>


namespace Ui {
class Form;
}

class Form : public QDialog
{
    Q_OBJECT

signals:
    void sendUser(QString user);

public:
    explicit Form(QWidget *parent = 0);
    ~Form();


    QStringList validNicks;
    QString nick;
    int con;

private:
     Ui::Form *ui;

     void inf_for_connect_to_db();
     void connecting();
     void react_on_combo();
     void change_var();
     void disconnect();

     QSqlDatabase customers;
     QSqlQuery *query;
     QString password, host;
     int port;

};


#endif // DBCONNECT_H
