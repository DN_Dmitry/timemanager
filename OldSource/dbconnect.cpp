#include "dbconnect.h"
#include "ui_dbconnect.h"


Form::Form(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);

    connect(ui->pbConnect, &QPushButton::clicked, this, &Form::inf_for_connect_to_db);
    connect(ui->pbConnect, &QPushButton::clicked, this, &Form::close);
    connect(ui->cbNick, &QComboBox::currentTextChanged, this, &Form::change_var);

    connecting();
    react_on_combo();

}

void Form::connecting()
{
    QSettings *settings = new QSettings("settings.ini", QSettings::IniFormat);
    customers = QSqlDatabase::addDatabase("QMYSQL", "mydb");
    nick = settings->value("data/UserName").toString();
    password = settings->value("data/Password").toString();
    host = settings->value("data/Adress").toString();
    port = settings->value("data/Port").toInt();
    //qDebug() << nick << password << host << port;
    customers.setUserName(nick);
    customers.setPassword(password);
    customers.setHostName(host);
    customers.setPort(port);
    if(customers.open())
    {
        query = new QSqlQuery(customers);
        query->exec("CREATE DATABASE DBptmt");
        query->exec("USE DBptmt");
        query->exec("CREATE TABLE customers(nickname VARCHAR(20))");
        con = 0;
        return;
    }
    QMessageBox *error = new QMessageBox();
    error->setInformativeText("Can't connect to database with these options.");
    error->setIcon(QMessageBox::Critical);
    error->exec();
    //qDebug() << customers.lastError();
    con = 1;
    return;
}

void Form::react_on_combo()
{
    query->exec("select * from customers;");
    query->first();
    for(int i = 0; i < query->size(); i++)
    {
        validNicks.push_back(query->value(0).toString());
        qDebug() << "validNicks" << validNicks;
        query->next();
    }    
    ui->cbNick->addItems(validNicks);


    nick = ui->cbNick->currentText();
    customers.close();
}

void Form::change_var()
{
    nick = ui->cbNick->currentText();
}

void Form::inf_for_connect_to_db()
{
    //bool ok;

    /*QSettings *settings = new QSettings("settings.ini", QSettings::IniFormat);
    settings->setValue("data/UserName", "root");
    settings->setValue("data/Password", "supervis29");
    settings->setValue("data/Adress", "localhost");
    settings->setValue("data/Port", "3306");
    settings->sync();*/


    ui->pbConnect->setEnabled(false);

}

void Form::disconnect()
{
    delete query;
    customers.close();
}

Form::~Form()
{
    disconnect();
    delete ui;
}
