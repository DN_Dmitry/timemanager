#include "ptmt_main_wnd.h"
#include "ui_main_form.h"
#include "dbconnect.h"


ptmt_main_wnd::ptmt_main_wnd(QString str1, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ptmt_main_wnd)
{
    ui->setupUi(this);       
    ui->tbList->setColumnWidth(2, 180);

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &ptmt_main_wnd::display_time);
    timer->start(1000); //provides a change QTime readings over time (second)

    currentDate = QDate::currentDate();
    controlCalendar = QDate::currentDate();
    ui->cCalendar->setSelectedDate(currentDate);
    ui->cCalendar->setMaximumDate(currentDate);

    connect(ui->pbBegin, &QPushButton::clicked, this, &ptmt_main_wnd::react_on_begin);
    connect(ui->pbEnd, &QPushButton::clicked, this, &ptmt_main_wnd::react_on_end);
    connect(ui->cCalendar, &QCalendarWidget::clicked, this, &ptmt_main_wnd::change_date);
    connect(ui->pbAddCust, &QPushButton::clicked, this, &ptmt_main_wnd::add_customer);
    connect(ui->pbComment, &QPushButton::clicked, this, &ptmt_main_wnd::react_on_comment);
    connect(ui->pbPrint, &QPushButton::clicked, this, &ptmt_main_wnd::react_on_print);
    connect(ui->pbDelete, &QPushButton::clicked, this, &ptmt_main_wnd::react_on_delete);


    checking_buttons_status();


    QSettings *settings1 = new QSettings("settings.ini", QSettings::IniFormat);
    userName = settings1->value("data/UserName").toString();
    password = settings1->value("data/Password").toString();
    host = settings1->value("data/Adress").toString();
    port = settings1->value("data/Port").toInt();
    connecting = connect_to_db();
    nick = str1;    
    selDate = currentDate.toString("dd.MM.yyyy");

}

void ptmt_main_wnd::react_on_print()// print current table to pdf file
{
    QPrinter printer( QPrinter::HighResolution );
    printer.setOrientation( QPrinter::Portrait );
    printer.setOutputFormat(QPrinter::PdfFormat);
    if(!QDir("Reports").exists())
        QDir().mkdir("Reports");

    printer.setOutputFileName("Reports\\" + nick + selDate + ".pdf");
    QString html;

    QWebView *webView = new QWebView(); // create report object
    webView->setStyleSheet( "background-color: #fff;" );

    QPrintPreviewDialog preview( &printer, this );
    preview.setWindowFlags( Qt::Window );

    QObject::connect( &preview, SIGNAL( paintRequested( QPrinter* ) ), webView, SLOT( print( QPrinter* ) ) );

    preview.setModal( true );
    preview.setOrientation( Qt::Horizontal );
    preview.setMinimumSize( 1000, 1000 ); // size preview window


    preview.setWindowTitle("Preview"); // title of preview window

    QList<QString> rowHeader;
    QString title = QTextCodec::codecForName("Windows-1251")->toUnicode("Daily report  ");

    html = QString("<p style='font-size: 18px; font-weight: bold;' align= center>&nbsp;%1 %2</p>").arg(title, selDate);
    html.append("<table align= center border='1' cellpadding='1'width=100% cellspacing='1' style=width: 1000px; margin-left: 10px;> <thead> <tr>");

    for( int i = 0; i < ui->tbList->columnCount(); i++ )
    {
        QTableWidgetItem *item = new QTableWidgetItem();
        item = ui->tbList->horizontalHeaderItem(i);
        rowHeader.append(item->text());

        html.append( QString( "<th scope= 'col' style='background-color: #66ff33;'>%1</th>" ).arg( rowHeader.at( i ) ) );
    }
    html.append( "</tr>" );
    html.append("</thead>");
    html.append("<tbody>");

    QList<QString> rowData;
    for( int i = 0; i < ui->tbList->rowCount(); i++ )
    {
        html.append( "<tr>" );
        for( int j = 0; j < ui->tbList->columnCount(); j++ )
        {
            rowData.append( ui->tbList->item( i, j )->text() );
            html.append( QString( "<td>%1</td>" ).arg( rowData.at( j ) ) );
        }

        rowData.clear();
        html.append( "</tr>" );
    }

    html.append( "</tbody></table><br />" );

    //
    html.append( QString( "<br /><span style='font-size: 18px; text-align: right;'><p><b>" ) +        
        QString("%1 </b></p></span><br /><br /><br /><br /><br /><br />" ).arg( QDateTime().currentDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ) );
    html.prepend( "<html style=\"background-color: #fff; padding-botom: 30px;\"><body>" );
    html.append( "</body></html>" );

    webView->setLocale(QLocale::Russian);
    webView->setHtml(html);

    preview.exec();
    webView->deleteLater();
}

void ptmt_main_wnd::add_customer()
{
    bool bOk;
    QString newNick = QInputDialog::getText(0, "Input", "Name:", QLineEdit::Normal, "", &bOk);
    if(newNick.isEmpty())
        return;
    if (!bOk)
        return;
    for(int i = 0; i < newNick.size(); i++)
    {
        if(newNick.at(i).isSpace())
        {
            QMessageBox *error = new QMessageBox();
            error->setInformativeText("Nickname can't contain a separator characters.");
            error->setIcon(QMessageBox::Critical);
            error->exec();
            delete error;
            return;
        }
    }
    q->exec("select * from customers;");
    q->first();

        for(int i = 0; i < q->size(); i++)
        {
            validNicks.push_back(q->value(0).toString());
            qDebug() << "validNicks" << validNicks;
            q->next();
        }
        qDebug() << "validNicks" << validNicks;
        if(validNicks.isEmpty())
            q->exec("insert into customers value('" + newNick + "');");
        for(auto it = validNicks.begin(); it != validNicks.end(); it++)
        {
            if(newNick != *it)
            {
                if(it == validNicks.end() - 1)
                {
                    q->exec("insert into customers value('" + newNick + "');");
                }
                continue;
            }
            if(newNick == *it)
            {
                QMessageBox *error = new QMessageBox();
                error->setInformativeText("This nickname is already in use.");
                error->setIcon(QMessageBox::Critical);
                error->exec();
                return;
            }
        }
}


bool ptmt_main_wnd::connect_to_db()
{   
    db = QSqlDatabase::addDatabase("QMYSQL", "mydb");    
    db.setUserName(userName);
    db.setPassword(password);
    db.setHostName(host);
    db.setPort(port);
    if(db.open())
    {
        qDebug() << "im connected!";
        q = new QSqlQuery(db);
        q->exec("CREATE DATABASE DBptmt");
        q->exec("USE DBptmt");
        q->exec("CREATE TABLE ptmt(Begin_time VARCHAR(10), End_time VARCHAR(10), Date VARCHAR(10), Summary VARCHAR(25), nickname VARCHAR(20))");
        q->exec("create table customers(nickname VARCHAR(30))");

        return true;
    }
    else
    {
        QMessageBox *error = new QMessageBox();
        error->setInformativeText("Can't connect to database with these options.");
        error->setIcon(QMessageBox::Critical);
        error->exec();
        qDebug() << db.lastError();
        return false;
    }

}

void ptmt_main_wnd::disconnect_db()
{
    db.close();
    delete q;
    qDebug() << "Im disconnect!";
}

void ptmt_main_wnd::record_to_db(const QDate &selectedDate)
{
    if(selectedDate == currentDate)
    {
        date = currentDate.toString("dd.MM.yyyy");
        for(int i = 0; i < ui->tbList->rowCount(); i++)
        {
            QString szBegin = ui->tbList->model()->index(i, TABLE_COLUMN).data(Qt::DisplayRole).toString();
            if(q->exec( "select distinct Begin_time from ptmt where Begin_time = '" + szBegin + "' and Date = '" + date + "';"));
                q->exec("delete from ptmt where Begin_time = '" + szBegin + "' and Date = '" + date + "';");
            QString szEnd = ui->tbList->model()->index(i, TABLE_COLUMN + 1).data(Qt::DisplayRole).toString();
            QString szSummary = ui->tbList->model()->index(i,TABLE_COLUMN + 2).data(Qt::DisplayRole).toString();
            q->exec("insert into ptmt values('" + szBegin + "', '" + szEnd + "', '" + date + "', '" + szSummary + "', '" + nick + "')");

        }
    }
}

void ptmt_main_wnd::read_from_db(const QDate &selectedDate)
{       
    QString tagDate = "'" + selectedDate.toString("dd.MM.yyyy") + "'";
    ui->tbList->clearContents();
    ui->tbList->setRowCount(0);
    q->exec("select distinct Begin_time, End_time, Summary  from ptmt where Date = " + tagDate + " and nickname = '" + nick + "' order by Begin_time desc;");
    q->first();
    ui->tbList->insertRow(TABLE_ROW);
    //qDebug() << "q: " << q->lastError();
    for (int i=0; i<3; i++)
    {
        QTableWidgetItem * item = new QTableWidgetItem(q->value(i).toString());        
        ui->tbList->setItem(ui->tbList->rowCount()- 1,i,item);
        item->setFlags(item->flags() & ~Qt::ItemIsEditable);
    }
    while (q->next())
    {
        ui->tbList->insertRow(ui->tbList->rowCount());
        for (int i=0; i<3; i++)
        {
            QTableWidgetItem * item = new QTableWidgetItem(q->value(i).toString());
            ui->tbList->setItem(ui->tbList->rowCount()- 1,i,item);
            item->setFlags(item->flags() & ~Qt::ItemIsEditable);
        }
    }

}

void ptmt_main_wnd::react_on_comment()
{
    bool bOk;
    comment = QInputDialog::getText(0, "Input", "Comment:", QLineEdit::Normal, "", &bOk);
    if(comment.isEmpty())
        return;
    QTableWidgetItem *item2 = new QTableWidgetItem();
    item2->setText(comment);
    ui->tbList->setItem(ui->tbList->currentRow(), TABLE_COLUMN + 2, item2);  //entry comment to QTableWidget
    item2->setFlags(item2->flags() & ~Qt::ItemIsEditable);
    comment = "";
    if(!bOk)
        return;
}


void ptmt_main_wnd::react_on_begin() //react on click "Begin"
{    
    begin = QTime::currentTime(); // obtaining the current time
    QString szBeginTime = begin.toString("hh:mm:ss"); //current date and time be converting to QString    

    ui->tbList->insertRow(TABLE_ROW); //create a new row in QTableWidget
    //create_button();

    QTableWidgetItem *item1 = new QTableWidgetItem();
    item1->setText(szBeginTime);
    ui->tbList->setItem(TABLE_ROW, TABLE_COLUMN, item1);  //entry current time to QTableWidget
    item1->setFlags(item1->flags() & ~Qt::ItemIsEditable);    


    analyze_table();
    checking_buttons_status();    

}

void ptmt_main_wnd::react_on_end() //react on click "End"
{    
    if(ui->tbList->item(TABLE_ROW, TABLE_COLUMN) != 0)
    {
        end = QTime::currentTime(); // obtaining the current time
        QString szEndTime = end.toString("hh:mm:ss"); // time converting to string

        QTableWidgetItem *item = new QTableWidgetItem();
        item->setText(szEndTime);
        ui->tbList->setItem(TABLE_ROW, TABLE_COLUMN + 1, item); //entry current time to QTableWidget
        item->setFlags(item->flags() & ~Qt::ItemIsEditable);

        analyze_table();
        checking_buttons_status();


    }

}

void ptmt_main_wnd::display_time() //clock on a main form
{
    QDate date = QDate::currentDate(); // obtaining the current date
    QTime time = QTime::currentTime();  // obtaining the current time
    QString szTime = time.toString("hh:mm:ss"); //converting time to QString
    QString szDate = date.toString("dd/MM/yy"); //converting date to QString    
    ui->lbClock->setText(szDate + "   " + szTime); //output

    if(szTime == "23:59:00")
        control_midnight_end();
    if(szTime == "00:00:00")
        control_midnight_begin();
}

void ptmt_main_wnd::control_midnight_end()
{
    if(ui->cCalendar->selectedDate() != currentDate)
    {
        ui->cCalendar->setSelectedDate(currentDate);
        read_from_db(currentDate);
        analyze_table();
        if(ui->tbList->item(TABLE_ROW, TABLE_COLUMN + 1) == 0)
            react_on_end();
        record_to_db(currentDate);
        ui->tbList->clearContents();
        ui->tbList->setRowCount(TABLE_ROW);
    }
    else
    {
        if(ui->tbList->item(TABLE_ROW, TABLE_COLUMN + 1) == 0)
        {
            react_on_end();
            ui->pbBegin->setEnabled(false);
            ui->pbEnd->setEnabled(false);
        }
        record_to_db(currentDate);
    }
}

void ptmt_main_wnd::control_midnight_begin()
{
    currentDate = QDate::currentDate();
    controlCalendar = QDate::currentDate();
    ui->tbList->clearContents();
    ui->tbList->setRowCount(TABLE_ROW);
    ui->cCalendar->setMaximumDate(currentDate);
    ui->cCalendar->setSelectedDate(currentDate);
    react_on_begin();
    checking_buttons_status();
}


void ptmt_main_wnd::calculate_efficiency()
{
    float secWork = workTime / 1000;
    float secBreak = breakTime / 1000;
    efficiency = 100 * (secWork / (secWork + secBreak));

    ui->lbEff->setText(QString::number(efficiency) + "%"); //displaied coefficient of efficiency
}

void ptmt_main_wnd::checking_buttons_status()
{
    ui->pbComment->setEnabled(true);
    if(ui->tbList->item(TABLE_ROW, TABLE_COLUMN) != 0 && ui->tbList->item(TABLE_ROW, TABLE_COLUMN + 1) == 0)// "Begin" is pressed
    {
        ui->pbBegin->setEnabled(false);
        ui->pbEnd->setEnabled(true);
        ui->pbComment->setEnabled(true);

    }
    if(ui->tbList->item(TABLE_ROW, TABLE_COLUMN) == 0 && ui->tbList->item(TABLE_ROW, TABLE_COLUMN + 1) == 0) // main table is empty
    {
        ui->pbBegin->setEnabled(true);
        ui->pbEnd->setEnabled(false);
        ui->pbComment->setEnabled(false);
    }
    else
        if(ui->tbList->item(TABLE_ROW, TABLE_COLUMN) != 0 && ui->tbList->item(TABLE_ROW, TABLE_COLUMN + 1) != 0)// "Begin" and "End" is pressed
        {
            ui->pbBegin->setEnabled(true);
            ui->pbEnd->setEnabled(false);
            ui->pbComment->setEnabled(true);
        }    
}

void ptmt_main_wnd::change_date(const QDate &date)
{    
    selDate = date.toString("dd.MM.yyyy");
    if(ui->cCalendar->selectedDate() != currentDate)//customer clicked on other date
    {        
        ui->cCalendar->setSelectedDate(date);        
        if(date != currentDate)
        {
            if(ui->tbList->item(TABLE_ROW, TABLE_COLUMN + 1) == 0)
                react_on_end();
            record_to_db(controlCalendar); // App will wrote information from table to database
            ui->pbBegin->setEnabled(false);
            ui->pbEnd->setEnabled(false);// buttons are disabled
            ui->pbComment->setEnabled(false);
            read_from_db(date);// reading information by selected date from database and inputing it to table
            analyze_table();            
            controlCalendar = date;
        }
        else
        {
            read_from_db(date);           
            analyze_table();
            checking_buttons_status();
            //come back to real buttons status
        }
    }
    else//customer clicked on today's date
    {        
        ui->cCalendar->setSelectedDate(date);
        if(date != currentDate)
        {
            ui->pbBegin->setEnabled(false);
            ui->pbEnd->setEnabled(false);
            read_from_db(date);
            analyze_table();

        }
        else
        {
            read_from_db(date);
            analyze_table();
            checking_buttons_status();            
            controlCalendar = currentDate;
        }
    }
}

void ptmt_main_wnd::analyze_table()
{
    int RowCount = ui->tbList->rowCount() - 1; //real count of table rows
    workTime = 0;
    breakTime = 0;
    for(int i = 0; i < ui->tbList->rowCount(); i++)
    {
        QString szBegin = ui->tbList->model()->index(RowCount, TABLE_COLUMN).data(Qt::DisplayRole).toString();
        begin = QTime::fromString(szBegin, "hh:mm:ss");
        QString szEnd = ui->tbList->model()->index(RowCount, 1).data(Qt::DisplayRole).toString();
        end = QTime::fromString(szEnd, "hh:mm:ss");// convert "end" & "begin" values for one row from QString to QTime

        int interimTime;
        interimTime = end.msecsTo(begin);//amount miliseconds (int)
        workTime -= interimTime; //calculate total count of miliseconds between "end" & "begin" values

        QTime spent_time(-interimTime / HOURS, (-interimTime % HOURS) / MINUTES, (-interimTime % MINUTES) / SECONDS, -interimTime % SECONDS);
        QString szSpentTimeForTable = spent_time.toString("hh:mm:ss");
        QTableWidgetItem *item = new QTableWidgetItem();
        item->setText(szSpentTimeForTable);
        ui->tbList->setItem(RowCount, TABLE_COLUMN + 3, item);//calculate & display spent time for current task
        item->setFlags(item->flags() & ~Qt::ItemIsEditable);

        QTime workspent(workTime / HOURS, (workTime % HOURS) / MINUTES, (workTime % MINUTES) / SECONDS, workTime % SECONDS);
        QString szSpent = workspent.toString("hh:mm:ss");
        ui->lbTW->setText(szSpent);//display total work time on label

        szBegin = ui->tbList->model()->index(RowCount - 1, 0).data(Qt::DisplayRole).toString();
        begin = QTime::fromString(szBegin, "hh:mm:ss");
        szEnd = ui->tbList->model()->index(RowCount, 1).data(Qt::DisplayRole).toString();
        end = QTime::fromString(szEnd, "hh:mm:ss");

        interimTime = begin.msecsTo(end);//amount miliseconds (int)
        breakTime -= interimTime;

        QTime breakspent(breakTime / HOURS, (breakTime % HOURS) / MINUTES, (breakTime % MINUTES) / SECONDS, breakTime % SECONDS);
        szSpent = breakspent.toString("hh:mm:ss");
        ui->lbTB->setText(szSpent);//display total break time on label
        if(ui->tbList->item(1, 1) != 0)
            calculate_efficiency();
        RowCount--;
    }
    if((workTime >= HOURS*3) && (ui->cCalendar->selectedDate() == currentDate))
    {
        QMessageBox alarmClock;
        alarmClock.setInformativeText("You've been working more than three hours today! 'Alternate Sunrise' company thanks you for your work! Employees like you are the epitome of professionalism! We wish you a pleasant holiday!");
        alarmClock.setIcon(QMessageBox::Information);
        alarmClock.exec();
    }
}

void ptmt_main_wnd::react_on_delete()
{
    QString szCom = ui->tbList->model()->index(ui->tbList->currentRow(), TABLE_COLUMN + 2).data(Qt::DisplayRole).toString();
    q->exec("delete from ptmt where Summary = '" + szCom + "';");//it will be DELETE sql-command
    ui->tbList->removeRow(ui->tbList->currentRow());
}

ptmt_main_wnd::~ptmt_main_wnd()//destructor
{    
    if(ui->cCalendar->selectedDate() == currentDate)
    {
        if(ui->tbList->item(TABLE_ROW, TABLE_COLUMN + 1) == 0)
            react_on_end();
        record_to_db(currentDate);
    }
    disconnect_db();
    delete timer;
    delete ui;
}
