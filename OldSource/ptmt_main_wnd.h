#ifndef PTMT_MAIN_WND_H
#define PTMT_MAIN_WND_H

#include <QMainWindow>

#include <QDate>
#include <QTime>
#include <QTimer>
#include <QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QMessageBox>
#include <QWebView>
#include <QSettings>
#include <QInputDialog>
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QPrintPreviewDialog>
#include <QInputDialog>



#define TABLE_ROW 0
#define TABLE_COLUMN 0
#define SECONDS 1000
#define MINUTES 60000
#define HOURS 3600000


namespace Ui {
class ptmt_main_wnd;
}

class ptmt_main_wnd : public QMainWindow
{
    Q_OBJECT

public:
    explicit ptmt_main_wnd(QString str1, QWidget *parent = 0);
    ~ptmt_main_wnd();

    QString userName;
    QString password;
    QString host;
    int port;
    bool connect_to_db();
    bool connecting;



private:
    Ui::ptmt_main_wnd *ui;

    void display_time();
    void react_on_begin();
    void react_on_end();
    void calculate_efficiency();
    void create_button();
    void checking_buttons_status();
    void change_date(const QDate &date);
    void analyze_table();
    void disconnect_db();
    void record_to_db(const QDate &selectedDate);
    void read_from_db(const QDate &selectedDate);
    void control_midnight_end();
    void control_midnight_begin();
    void add_customer();
    void react_on_comment();
    void react_on_print();
    void react_on_delete();
    void checking_working_time();




    float efficiency;
    long time;
    long breakTime;
    long workTime;
    QTimer *timer;
    QTime begin;
    QTime end;
    QTime spent;
    QDate currentDate;
    QDate controlCalendar;
    QSqlDatabase db;
    QSqlQuery *q;
    QString date;
    QString nick;
    QString comment;
    QStringList validNicks;
    QString selDate;


};

#endif // PTMT_MAIN_WND_H
